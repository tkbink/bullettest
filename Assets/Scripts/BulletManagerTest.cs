using System.Collections;
using System.Collections.Generic;
using UnityEngine;




/// <summary>
/// Implement the following class. This class manages the game objects used as bullets in a game.
/// Assume that there will be a need to spawn a lot of bullets as efficiently as possible.
/// You have 1 day to work on it.
/// </summary>
public class BulletManagerTest : MonoBehaviour
{
	//Prefab for a single bullet
	public GameObject BulletPrefab;
	//TODO - declar the neccessary member variables
    public List<BulletController> bulletList;
    public static BulletManagerTest Instance = null;

	void Start()
	{
        //TODO - system initailization
        if (Instance == null)
        {
            Instance = this;
        }
	}

	/// <summary>
	/// Spawn bullets
	/// </summary>
	/// <param name="count">number of bullets to spawn</param>
	/// <param name="aim">starting position and direction of the bullets</param>
	/// <param name="lifeTime">life time of bullets in seconds</param>
	/// <param name="force">how fast the bullets should travel per second</param>
	/// <param name="maxSpreadAngle">how wide the bullets can spread in degree</param>
	public void SpawnBullets(int count, Ray aim, float lifeTime, float force, float maxSpreadAngle)
	{

      
		//TODO - spawn the bullets according to the input parameters
        for (int i = 0; i < count; i++)
        {
            Vector3 randomAngle = new Vector3(0, Random.Range(maxSpreadAngle * -1, maxSpreadAngle), 0);
            GameObject bullets = Instantiate(BulletPrefab, aim.origin, Quaternion.Euler(randomAngle));
            BulletController ctrl = bullets.GetComponent<BulletController>();
            ctrl.force = force;
            ctrl.countTime = lifeTime;
            ctrl.endPoint = aim.direction;
            bulletList.Add(ctrl);

        }

	}

	private void UpdateBullets()
	{
        //TODO - update the position of the bullets
        foreach (var bullets in bulletList)
        {
            bullets.GetComponent<Rigidbody>().velocity = bullets.force * (bullets.transform.forward*bullets.endPoint.z);
            bullets.transform.localPosition += (bullets.transform.forward*bullets.endPoint.z);   
        }

    }

	private void DrawBullets()
	{
        //TODO - visualize the bullets

    }

	void Update()
	{
		UpdateBullets();
		DrawBullets();

		//For testing
		if (Input.GetKeyDown(KeyCode.W))
		{
            SpawnBullets(50, new Ray(Vector3.zero, Vector3.forward), 3, 100, 10);

        }
        if (Input.GetKeyDown(KeyCode.S))
		{
			SpawnBullets(50, new Ray(Vector3.zero, Vector3.back), 3, 100, 10);
		}
	}

   
}



