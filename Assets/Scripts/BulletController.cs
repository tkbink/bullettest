﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
    public float force;
    public Vector3 endPoint;
    public float countTime;
    public float tempTime;

	// Use this for initialization
	void Start () {
      
	}
	
	// Update is called once per frame
	void Update () {
        tempTime += Time.deltaTime;

        if (tempTime >= countTime) {
            BulletManagerTest.Instance.bulletList.Remove(this);
            Destroy(this.gameObject);
        }
      
	}


}
